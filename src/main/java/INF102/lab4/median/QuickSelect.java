package INF102.lab4.median;

import java.util.ArrayList;
import java.util.List;

public class QuickSelect {
    public static <T extends Comparable<T>> T nthLowest(List<T> list, int n) {
        return nthLowest(new ArrayList<>(list), 0, list.size()-1, n);
    }

    private static <T extends Comparable<T>> T nthLowest(List<T> list, int start, int end, int n) {
        var partition = partition(list, start, end);
        if (partition < n) return nthLowest(list, partition+1, end, n);
        if (partition > n) return nthLowest(list, start, partition-1, n);
        return list.get(partition);
    }

    private static <T extends Comparable<T>> int partition(List<T> list, int start, int end) {
        var pivot = list.get(end);
        var ptr = start;

        for (var i = start; i <= end; i++) {
            if (list.get(i).compareTo(pivot) > 0) {
                swap(list, i, ptr);
                ptr++;
            }
        }

        swap(list, end, ptr);
        return ptr;
    }

    private static <T> void swap(List<T> list, int index0, int index1) {
        var tmp = list.get(index1);
        list.set(index1, list.get(index0));
        list.set(index0, tmp);
    }
}
