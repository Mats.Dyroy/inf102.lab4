package INF102.lab4.median;

import java.util.List;

public class QuickMedian implements IMedian {

    @Override
    public <T extends Comparable<T>> T median(List<T> list) {
        return QuickSelect.nthLowest(list, list.size() / 2);
    }
}