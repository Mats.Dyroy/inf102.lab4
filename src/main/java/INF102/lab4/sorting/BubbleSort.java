package INF102.lab4.sorting;

import java.util.List;

public class BubbleSort implements ISort {

    @Override
    public <T extends Comparable<T>> void sort(List<T> list) {
        var size = list.size();

        for (var i = 0; i < size; i++) {
            for (var j = 1; j < size - i; j++) {
                if (list.get(j - 1).compareTo(list.get(j)) > 0) {
                    var tmp = list.get(j - 1);
                    list.set(j - 1, list.get(j));
                    list.set(j, tmp);
                }
            }
        }
    }
    
}